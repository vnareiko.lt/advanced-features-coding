package advanced.features.coding.exercises;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Exercise28<E> extends ArrayList<E> {
    public static void main(String[] args) {
        Exercise28<String> e = new Exercise28<>() {{
            add("a");
            add("b");
            add("c");
            add("d");
            add("e");
            add("f");
            add("g");
        }};

        System.out.println(e.getEveryNthElement(2, 3));
    }

    public List<E> getEveryNthElement(int startIndex, int skip) {
        List<E> newList = this.stream().skip(startIndex).collect(Collectors.toList());

        return IntStream
                .range(0, newList.size()).filter(i -> i % (skip + 1) == 0)
                .mapToObj(newList::get).collect(Collectors.toList());
    }
}
