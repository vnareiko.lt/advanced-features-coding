package advanced.features.coding.exercises;

public class Exercise35 {
    public static void main(String[] args) {
        MyRunnable r = new MyRunnable();
        Thread t = new Thread(r);
        t.start();

        Thread t1 = new Thread(() -> System.out.println(Thread.currentThread().getName()));
        t1.start();
    }
}
