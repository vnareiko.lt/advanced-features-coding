package advanced.features.coding.exercises;

public class Hexagon extends Shape {
    private double a;

    public Hexagon(double a) {
        this.a = a;
    }


    @Override
    public double calculatePerimeter() {
        return a * 6.0;
    }

    @Override
    public double calculateArea() {
        return 3.0 * Math.sqrt(3.0) / 2.0 * Math.pow(a, 2.0);
    }
}
