package advanced.features.coding.exercises;

public class Exercise23 {
    public static void main(String[] args) {
        Zoo z = new Zoo();

        z.addAnimals("Wolf", 3);
        z.addAnimals("Leopard", 2);

        System.out.println(z.getNumberOfAllAnimals());
        System.out.println(z.getAnimalsCount());
    }
}
