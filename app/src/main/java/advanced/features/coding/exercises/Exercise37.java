package advanced.features.coding.exercises;

import java.util.concurrent.Executors;

public class Exercise37 {
    public static void main(String[] args) {
        var e = Executors.newFixedThreadPool(2);

        for (int i = 0; i < 10; i++) {
            e.execute(new ThreadPlaygroundRunnable("T" + i));
        }

        e.shutdown();
    }
}
