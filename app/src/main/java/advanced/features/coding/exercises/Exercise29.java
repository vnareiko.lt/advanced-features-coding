package advanced.features.coding.exercises;

import java.util.Arrays;
import java.util.function.Predicate;

public class Exercise29<T> {
    public static void main(String[] args) {
        Exercise29<Integer> e = new Exercise29<>();
        System.out.println(e.partOf(new Integer[]{1, 2, 3, 4, 5, 6}, i -> i > 3));
    }

    public int partOf(T[] array, Predicate<T> predicate) {
        long elementCount = Arrays.stream(array).filter(predicate).count();
        return (int) ((double) elementCount / array.length * 100);
    }
}
