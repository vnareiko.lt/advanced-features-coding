package advanced.features.coding.exercises;

public class Exercise16 {
    public static void main(String[] args) {
        System.out.println(Runner.getFitnessLevel(60));
    }

    public static enum Runner {
        UNKNOWN(Integer.MIN_VALUE, Integer.MAX_VALUE),
        BEGINNER(100, 150),
        INTERMEDIATE(80, 100),
        ADVANCED(60, 70);

        private final int min;
        private final int max;

        Runner(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public static Runner getFitnessLevel(int time) {
            if (time >= BEGINNER.min && time <= BEGINNER.max) {
                return BEGINNER;
            }

            if (time >= INTERMEDIATE.min && time <= INTERMEDIATE.max) {
                return INTERMEDIATE;
            }

            if (time >= ADVANCED.min && time <= ADVANCED.max) {
                return ADVANCED;
            }

            return UNKNOWN;
        }
    }
}
