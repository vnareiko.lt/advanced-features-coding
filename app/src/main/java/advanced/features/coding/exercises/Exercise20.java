package advanced.features.coding.exercises;

public class Exercise20 {
    public static void main(String[] args) {
        Shape t = new Triangle(5.0, 3.0, 4.0);
        System.out.println(t.calculatePerimeter());
        System.out.println(t.calculateArea());

        Shape r = new Rectangle(5.0, 3.0);
        System.out.println(r.calculatePerimeter());
        System.out.println(r.calculateArea());

        Shape h = new Hexagon(5.0);
        System.out.println(h.calculatePerimeter());
        System.out.println(h.calculateArea());
    }
}
