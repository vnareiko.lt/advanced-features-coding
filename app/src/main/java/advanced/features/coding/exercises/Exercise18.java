package advanced.features.coding.exercises;

public class Exercise18 {
    public static void main(String[] args) {
        Computer pc1 = new Computer("Intel", "A-Data", "Radeon", "Gigabyte", "Asus", "RX");
        Computer pc2 = new Computer("Intel", "A-Data", "Radeon", "Gigabyte", "Asus", "RX");

        System.out.println(pc1.equals(pc2));
    }
}
