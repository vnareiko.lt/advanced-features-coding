package advanced.features.coding.exercises;

import java.util.Random;

public class Screen implements Runnable {
    private static final Random random = new Random();
    private final Competition competition;

    public Screen(Competition competition) {
        this.competition = competition;
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.printf("Screen %s got result %f%n", Thread.currentThread().getName(), competition.readResult());
                Thread.sleep(random.nextInt(10000));
            }
        } catch (InterruptedException ignored) {

        }
    }
}
