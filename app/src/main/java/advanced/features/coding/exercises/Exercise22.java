package advanced.features.coding.exercises;

public class Exercise22 {
    public static void main(String[] args) {
        Shape3D cone = new Cone(5.0, 6.0);
        System.out.println(cone.calculateVolume());
        System.out.println(cone.fill(157.07963267948963));
    }
}
