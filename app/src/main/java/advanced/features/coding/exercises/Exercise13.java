package advanced.features.coding.exercises;

import java.util.Arrays;

public class Exercise13 {
    public static void main(String[] args) {
        Manufacturer m1 = new Manufacturer(
                "Fiat", 1999, "Italy");
        Manufacturer m2 = new Manufacturer(
                "Fiat", 1998, "Italy");

        Car c1 = new Car(
                "Fiat",
                "Bravo",
                500.0,
                1998,
                Arrays.asList(m1, m2),
                Engine.V6
        );

        Car c2 = new Car(
                "Fiat",
                "Punto",
                600.0,
                2000,
                Arrays.asList(m1, m2),
                Engine.V12
        );

        CarService carService = new CarService();

        carService.add(c1);
        carService.add(c2);

//        System.out.println(carService.findByV12());
//        System.out.println(carService.findBefore1999());
//        System.out.println(carService.findMostExpensive());
        System.out.println(carService.findCheapest());
    }
}
