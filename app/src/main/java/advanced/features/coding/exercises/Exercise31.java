package advanced.features.coding.exercises;

import com.google.common.base.Strings;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Exercise31 {
    public static void main(String[] args) throws IOException {
        Map<String, Integer> wordCounter = new HashMap<>();

        Files.lines(Path.of("../advanced-features-coding/words.txt"))
                .flatMap(l -> Arrays.stream(l.split(" ")))
                .forEach(w -> {
                    if (wordCounter.containsKey(w)) {
                        wordCounter.put(w, wordCounter.get(w) + 1);
                    } else {
                        wordCounter.put(w, 1);
                    }
                });

        StringBuilder sb = new StringBuilder();
        sb.append(Strings.repeat("-", 23)).append("\n");

        wordCounter.forEach((key, value) ->
                sb
                        .append("|")
                        .append(Strings.padStart(key, 10, ' '))
                        .append("|")
                        .append(Strings.padStart(value.toString(), 10, ' '))
                        .append("|")
                        .append("\n")
                        .append(Strings.repeat("-", 23))
                        .append("\n"));

        Files.write(Path.of("../advanced-features-coding/words2.txt"), Collections.singleton(sb.toString()), Charset.defaultCharset());
    }
}
