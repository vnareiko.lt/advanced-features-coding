package advanced.features.coding.exercises;

public class Exercise36 {
    private static Thread t1;
    private static Thread t2;

    public static void main(String[] args) throws InterruptedException {
        t1 = new Thread(new ThreadPlaygroundRunnable("T1"));
        t2 = new Thread(new ThreadPlaygroundRunnable("T2"));

        t1.start();
        t2.start();
    }
}
