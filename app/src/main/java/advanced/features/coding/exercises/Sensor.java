package advanced.features.coding.exercises;

import java.util.Random;

public class Sensor implements Runnable {
    private static final Random random = new Random();
    private final Competition competition;

    public Sensor(Competition competition) {
        this.competition = competition;
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.printf("Sensor %s updating result%n", Thread.currentThread().getName());
                competition.updateResult(competition.readResult(), random.nextDouble());
                Thread.sleep(random.nextInt(10000));
            }
        } catch (InterruptedException ignored) {

        }
    }
}
