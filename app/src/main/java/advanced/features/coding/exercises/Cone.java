package advanced.features.coding.exercises;

public class Cone extends Shape3D {
    private double r;
    private double h;

    public Cone(double r, double h) {
        this.r = r;
        this.h = h;
    }

    @Override
    public double calculatePerimeter() {
        return 0;
    }

    @Override
    public double calculateArea() {
        return Math.PI * r * Math.sqrt(Math.pow(r, 2.0) + Math.pow(h, 2.0));
    }

    @Override
    public double calculateVolume() {
        return 1.0 / 3.0 * Math.PI * Math.pow(r, 2.0) * h;
    }

    @Override
    public int fill(double w) {
        double v = calculateVolume();
        int level = 0;

        if (w == v) {
            return level;
        }

        if (w < v) {
            level = -1;
        }

        if (w > v) {
            level = 1;
        }

        return level;
    }
}
