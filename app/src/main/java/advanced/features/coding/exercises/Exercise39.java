package advanced.features.coding.exercises;

public class Exercise39 {
    public static void main(String[] args) {
        Competition c = new Competition();

        Sensor sen1 = new Sensor(c);
        Sensor sen2 = new Sensor(c);

        Screen scr1 = new Screen(c);
        Screen scr2 = new Screen(c);
        Screen scr3 = new Screen(c);

        Thread t1 = new Thread(sen1);
        Thread t2 = new Thread(sen2);

        Thread t3 = new Thread(scr1);
        Thread t4 = new Thread(scr2);
        Thread t5 = new Thread(scr3);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}
