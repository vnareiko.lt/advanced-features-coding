package advanced.features.coding.exercises;

public interface Resizable {
    void resize(double resizeFactor);
}
