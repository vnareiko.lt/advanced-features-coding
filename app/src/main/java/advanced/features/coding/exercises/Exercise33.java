package advanced.features.coding.exercises;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Exercise33 {
    public static void main(String[] args) throws IOException {
        Files.walk(Path.of(("/home/viktornareiko/Pictures")))
                .filter(f -> f.toString().endsWith(".png"))
                .forEach(System.out::println);
    }
}
