package advanced.features.coding.exercises;

import java.util.Scanner;

public class CoffeeMachine {
    private int water = 1200, milk = 500, coffeeBeans = 120, disposableCups = 10, money = 0;

    public int getWater() {
        return water;
    }

    public int getMilk() {
        return milk;
    }

    public int getCoffeeBeans() {
        return coffeeBeans;
    }

    public int getDisposableCups() {
        return disposableCups;
    }

    public int getMoney() {
        return money;
    }

    public boolean espresso(int cup) {
        if (250 * cup > water) {
            System.out.println("Not enough water resource, please refill.");
        }

        if (16 * cup > coffeeBeans) {
            System.out.println("Not enough coffee beans resource, please refill.");
        }

        if (cup > disposableCups) {
            System.out.println("Not enough cups resource, please refill.");
        }

        if (250 * cup < water && 16 * cup < coffeeBeans && cup < disposableCups) {
            System.out.println("Enough resource, making cup of coffee, please wait.\n");
            water -= 250 * cup;
            coffeeBeans -= 16 * cup;
            disposableCups -= cup;
            money += 4 * cup;
            remaining();
            return true;
        }

        return false;
    }

    public boolean latte(int cup) {
        if (350 * cup > water) {
            System.out.println("Not enough water resource, please refill.");
        }

        if (75 * cup > milk) {
            System.out.println("Not enough milk resource, please refill.");
        }

        if (20 * cup > coffeeBeans) {
            System.out.println("Not enough coffee beans resource, please refill.");
        }

        if (cup > disposableCups) {
            System.out.println("Not enough disposable cups resource, please refill.");
        }

        if (350 * cup < water && 75 * cup < milk && 20 * cup < coffeeBeans && cup < disposableCups) {
            System.out.println("Enough resource, making cup of coffee, please wait.\n");

            water -= 350 * cup;
            milk -= 75 * cup;
            coffeeBeans -= 20 * cup;
            disposableCups -= cup;
            money += 7 * cup;
            remaining();

            return true;
        }

        return false;
    }

    public boolean cappuccino(int cup) {
        if (200 * cup > water) {
            System.out.println("Not enough water resource, please refill");
        }

        if (100 * cup > milk) {
            System.out.println("Not enough milk resource, please refill");
        }

        if (12 * cup > coffeeBeans) {
            System.out.println("Not enough coffee beans resource, please refill.");
        }

        if (cup > disposableCups) {
            System.out.println("Not enough cups resource, please refill.");
        }

        if (200 * cup < water && 100 * cup < milk && 12 * cup < coffeeBeans && cup < disposableCups) {
            System.out.println("Enough resource, making cup of coffee, please wait.\n");
            water -= 200 * cup;
            milk -= 100 * cup;
            coffeeBeans -= 12 * cup;
            disposableCups -= cup;
            money += 6 * cup;
            remaining();

            return true;
        }

        return false;
    }

    public void fill() {
        this.water = 1200;
        this.milk = 500;
        this.coffeeBeans = 120;
        this.disposableCups = 10;
        remaining();
    }

    public void remaining() {
        System.out.println("\nResources in the coffee machine");
        System.out.println(water + " Water remaining in the machine");
        System.out.println(milk + " Milk remaining in the machine");
        System.out.println(coffeeBeans + " Coffee Beans remaining in the machine");
        System.out.println(disposableCups + " Disposable cups remaining in the machine");
        System.out.println("$" + money + " Money in the machine\n");
    }

    public boolean buy(String choice, int cup) {
        switch (choice) {
            case "espresso":
                return espresso(cup);
            case "latte":
                return latte(cup);
            case "cappuccino":
                return cappuccino(cup);
        }

        return false;
    }


    public static void main(String[] args) {
        System.out.println("Welcome please choose what would you like (buy, fill, remaining, exit)");
        Scanner input = new Scanner(System.in);
        String choice = input.next();

        CoffeeMachine cm = new CoffeeMachine();

        while (!choice.equals("exit")) {
            switch (choice) {
                case "buy":
                    System.out.println("Please enter coffee:\n");
                    String coffee = input.next();
                    cm.buy(coffee, 1);
                    break;
                case "fill":
                    cm.fill();
                    return;
                case "remaining":
                    cm.remaining();
                    break;
                default:
                    System.out.println("Wrong input, please try again (buy, fill, remaining, exit)\n");
                    break;
            }
            System.out.println("\nPlease choose what would you like (buy, fill, remaining, exit");
            choice = input.next();
        }
    }
}
