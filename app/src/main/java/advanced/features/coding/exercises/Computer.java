package advanced.features.coding.exercises;

import java.util.Objects;

public class Computer {
    private String processor;
    private String ram;
    private String graphics;
    private String card;
    private String company;
    private String model;

    public Computer(String processor, String ram, String graphics, String card, String company, String model) {
        this.processor = processor;
        this.ram = ram;
        this.graphics = graphics;
        this.card = card;
        this.company = company;
        this.model = model;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getGraphics() {
        return graphics;
    }

    public void setGraphics(String graphics) {
        this.graphics = graphics;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computer computer = (Computer) o;
        return Objects.equals(getProcessor(), computer.getProcessor()) && Objects.equals(getRam(), computer.getRam()) && Objects.equals(getGraphics(), computer.getGraphics()) && Objects.equals(getCard(), computer.getCard()) && Objects.equals(getCompany(), computer.getCompany()) && Objects.equals(getModel(), computer.getModel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProcessor(), getRam(), getGraphics(), getCard(), getCompany(), getModel());
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor='" + processor + '\'' +
                ", ram='" + ram + '\'' +
                ", graphics='" + graphics + '\'' +
                ", card='" + card + '\'' +
                ", company='" + company + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
