package advanced.features.coding.exercises;

import java.util.concurrent.Executors;

public class Exercise38 {
    public static void main(String[] args) {
        CoffeeMachine cm = new CoffeeMachine();

        var e = Executors.newFixedThreadPool(2);
        BrewingService latteBrewing = new BrewingService(cm, "latte", 1);
        BrewingService cappuccinoBrewing = new BrewingService(cm, "cappuccino", 1);
        BrewingService espressoBrewing = new BrewingService(cm, "espresso", 1);

        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                e.execute(latteBrewing);
            }

            if (i % 3 == 0) {
                e.execute(cappuccinoBrewing);
            }

            if (i % 5 == 0) {
                e.execute(espressoBrewing);
            }
        }

        e.shutdown();
    }

    private static class BrewingService implements Runnable {
        private final CoffeeMachine cm;
        private final String coffee;
        private final int cups;

        public BrewingService(CoffeeMachine cm, String coffee, int cups) {
            this.cm = cm;
            this.coffee = coffee;
            this.cups = cups;
        }

        @Override
        public void run() {
            try {
                if (!cm.buy(coffee, cups)) {
                    cm.fill();
                    Thread.currentThread().interrupt();
                }
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println("Refilled. Exiting.\n");
            }
        }
    }
}
