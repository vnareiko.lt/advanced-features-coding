package advanced.features.coding.exercises;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Zoo {
    private final List<String> animals = new ArrayList<>();

    public int getNumberOfAllAnimals() {
        return animals.size();
    }

    public Map<String, Integer> getAnimalsCount() {
        return animals.stream()
                .collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1)));
    }

    public void addAnimals(String kind, int count) {
        assert (!kind.isEmpty());
        assert (count > 0);

        for (int i = 0; i < count; i++) {
            animals.add(kind);
        }
    }
}
