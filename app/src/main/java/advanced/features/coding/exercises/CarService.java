package advanced.features.coding.exercises;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CarService {
    private List<Car> cars = new ArrayList<>();

    public void add(Car car) {
        cars.add(car);
    }

    public void remove(Car car) {
        cars.remove(car);
    }

    public List<Car> findAll() {
        return cars;
    }

    public List<Car> findByV12() {
        return cars.stream().filter(c -> c.getEngine() == Engine.V12).collect(Collectors.toList());
    }

    public List<Car> findBefore1999() {
        return cars.stream().filter(c -> c.getYear() < 1999).collect(Collectors.toList());
    }

    public Optional<Car> findMostExpensive() {
        return cars.stream().max(Comparator.comparing(Car::getPrice));
    }

    public Optional<Car> findCheapest() {
        return cars.stream().min(Comparator.comparing(Car::getPrice));
    }

    public List<Car> findAllWithMoreManufacturers() {
        return cars.stream().filter(c -> c.getManufacturers().size() > 2).collect(Collectors.toList());
    }

    public List<Car> sort(boolean ascending) {
        if (ascending) {
            cars.sort(Comparator.comparing(Car::getName).thenComparing(Car::getModel));
        }

        cars.sort(Comparator.comparing(Car::getName).reversed().thenComparing(Car::getModel).reversed());

        return cars;
    }

    public boolean exist(Car car) {
        return cars.contains(car);
    }

        public List<Car> findAllByManufacturer(Manufacturer manufacturer) {
            return cars.stream().filter(c -> c.getManufacturers().contains(manufacturer)).collect(Collectors.toList());
        }

    public List<Car> findAllByYearAndToken(int year, String eqToken) {
        return cars.stream().filter(c -> {
            if (eqToken.equals("<")) {
                return c.getYear() < year;
            }
            if (eqToken.equals("<=")) {
                return c.getYear() <= year;
            }
            if (eqToken.equals(">")) {
                return c.getYear() > year;
            }
            if (eqToken.equals(">=")) {
                return c.getYear() >= year;
            }
            return false;
        }).collect(Collectors.toList());
    }
}
