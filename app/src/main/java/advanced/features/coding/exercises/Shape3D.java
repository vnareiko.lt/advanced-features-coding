package advanced.features.coding.exercises;

public abstract class Shape3D extends Shape implements Fillable {
    public abstract double calculateVolume();
}
