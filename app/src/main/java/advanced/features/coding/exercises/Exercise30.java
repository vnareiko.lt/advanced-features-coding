package advanced.features.coding.exercises;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

public class Exercise30 {
    public static void main(String... args) throws IOException {
        File in = new File("../advanced-features-coding/words.txt");
        File out = new File(in.getParentFile()
                + "/"
                + new StringBuilder(in.getName().split("\\.")[0])
                .reverse()
                .append(".")
                .append(in.getName().split("\\.")[1]).toString()
        );


        List<String> reversed = Files.lines(in.toPath())
                .map(l -> new StringBuilder(l).reverse().toString()).collect(Collectors.toList());

        Files.write(out.toPath(), reversed);

        //        Old way with old io api
//        StringBuilder reverser = new StringBuilder();
//        List<String> lines = FileUtil.readData(in);
//        lines
//                .stream()
//                .map(l -> new StringBuilder(l).reverse().toString())
//                .forEach(l -> reverser.append(l).append("\n"));

//        FileUtil.writeData(out, reverser.toString());

    }
}
