package advanced.features.coding.exercises;

public class Basket {
    private int count;
    private static final int MAX_COUNT = 10;
    private static final int MIN_COUNT = 0;

    public void addToBasket() throws BasketFullException {
        if (count == MAX_COUNT) {
            throw new BasketFullException("Basket is full.");
        }
        count += 1;
    }

    public void removeFromBasket() throws BasketEmptyException {
        if (count == MIN_COUNT) {
            throw new BasketEmptyException("Basket is empty.");
        }
        count -= 1;
    }
}
