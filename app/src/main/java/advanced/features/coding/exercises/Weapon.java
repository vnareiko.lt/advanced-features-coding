package advanced.features.coding.exercises;

import java.util.Deque;
import java.util.LinkedList;

public class Weapon {
    private final int capacity;
    private final Deque<String> magazine = new LinkedList<>();

    public Weapon(int capacity) {
        this.capacity = capacity;
    }

    public void loadBullet(String bullet) {
        if (magazine.size() < capacity) {
            magazine.add(bullet);
        }
    }

    public boolean isLoaded() {
        return magazine.size() > 0;
    }

    public String shot() {
        if (magazine.isEmpty()) {
            return "Empty magazine.";
        }

        return String.format("Shot bullet: %s.", magazine.pollLast());
    }
}
