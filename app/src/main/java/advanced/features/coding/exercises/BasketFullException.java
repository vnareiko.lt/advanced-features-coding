package advanced.features.coding.exercises;

public class BasketFullException extends Exception {
    public BasketFullException(String message) {
        super(message);
    }
}
