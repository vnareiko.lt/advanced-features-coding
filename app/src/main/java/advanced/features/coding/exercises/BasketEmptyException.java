package advanced.features.coding.exercises;

public class BasketEmptyException extends Exception {
    public BasketEmptyException(String message) {
        super(message);
    }
}
