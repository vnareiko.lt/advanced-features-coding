package advanced.features.coding.exercises;

public class Competition {
    private double result;

    public synchronized void updateResult(double currentResult, double newResult) {
        if (result == currentResult) {
            this.result = newResult;
        }
    }

    public synchronized double readResult() {
        return result;
    }
}
