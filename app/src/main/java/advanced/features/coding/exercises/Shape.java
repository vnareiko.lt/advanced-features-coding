package advanced.features.coding.exercises;

public abstract class Shape {
    public abstract double calculatePerimeter();
    public abstract double calculateArea();
}
