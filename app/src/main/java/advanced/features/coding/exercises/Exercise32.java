package advanced.features.coding.exercises;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exercise32 {
    public static void main(String[] args) throws IOException {
        List<String> l = Stream.of(
          new Person("Test", "Test"),
          new Person("Test2", "Test2"),
          new Person("Test3", "Test3")
        ).map(Person::toString).collect(Collectors.toList());

        Files.write(Path.of("../advanced-features-coding/person.txt"), l);

        Files.lines(Path.of("../advanced-features-coding/person.txt")).map(s -> {
            String name = s.split(",")[0];
            String surname = s.split(",")[1].replaceAll("\\s", "");
            return new Person(name, surname);
        }).forEach(System.out::println);
    }
}
