package advanced.features.coding.exercises;

public interface Fillable {
    int fill(double w);
}
